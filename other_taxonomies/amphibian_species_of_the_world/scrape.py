'''
Script for downloading and scraping classification from Frost's Amphibian Species of the World site:
################################################################################
Note: Data presented in Amphibian Species of the World at http://research.amnh.org/vz/herpetology/amphibia/ were updated to be current with ASW's database as of 31 January, 2011.

Trademark Notice: "Amphibian Species of the World: An Online Reference" is a trademark of Darrel R. Frost and the American Museum of Natural History. Any other product or company names mentioned herein are the trademarks of their respective owners.

Copyright Notice: Copyright 1998-2011 Darrel R. Frost and American Museum of Natural History. All Rights Reserved. Each document delivered from this server or web site may contain other proprietary notices and copyright information relating to that document. The following citation should be used in any published materials which reference the web site.

Citation: Frost, Darrel R. 2011. Amphibian Species of the World: an online reference. Version 5.5 (31 January, 2011). Electronic Database accessible at http://research.amnh.org/vz/herpetology/amphibia/
American Museum of Natural History, New York, USA.
################################################################################
Script by Mark T. Holder reuse of the script is granted under the terms of the BSD 
    license (note that use of the data comes with the Copyright and citation notices 
    included above).
    
Usage involves creating a temp directory to hold the html (to avoid hammering 
the website to request the same file via multiple runs of the script). 
Downloading the "Amphibia" page and then invoking the script.
    $ mkdir tmp
    $ wget http://research.amnh.org/vz/herpetology/amphibia/?action=references&id=1 -O tmp/1.html
    $ python scrap.py tmp/1.html tmp > out.tre
or
    $ python scrap.py tmp/1.html tmp -t > out.csv
'''
import sys, os
from bs4 import BeautifulSoup
import requests
PREFIX = 'http://research.amnh.org/vz/herpetology/amphibia'

class Node(object):
    def __init__(self, name, children):
        self.name = name
        self.children = children
        self.parent = None
        for c in children:
            c.parent = self
    def write_newick(self, out):
        if self.children:
            out.write('(')
            for c in self.children:
                if c is not self.children[0]:
                    out.write(',')
                c.write_newick(out)
            out.write(')')
        out.write("'%s'" % self.name)
    def write_csv(self, out, num):
        if 'num' not in self.__dict__:
            self.num = num 
            num += 1
        if self.parent is None:
            pnum =  ''
        else:
            pnum = str(self.parent.num)
        out.write("%s,%s,%s\n" % (str(self.num), pnum, self.name))
        for c in self.children:
            num = c.write_csv(out, num)
        return num
            

def parse_taxon_html(html_doc, taxonomic_name, save_dir, level):
    soup = BeautifulSoup(html_doc)
    contained = []
    for a in soup.find_all('a'):
        n = None
        try:
            n = a.attrs['name']
        except:
            pass
        if n == 'contained-taxa':
            assert(len(contained) == 0)
            par = a.parent
            assert(par)
            next_ul = a.parent.find_next('ul')
            li_list = next_ul.find_all('li')
            assert(li_list)
            for item in li_list:
                #print 'item =', item
                link_list = item.find_all('a')
                assert(link_list)
                assert(len(link_list) > 0)
                first_link = link_list[0]
                link = first_link.attrs['href']
                taxon_list = [c for c in first_link.children]
                assert len(taxon_list) == 1
                taxon = taxon_list[0].string
                assert(link.startswith('./'))
                split_by_eq = link.split('=')
                #print split_by_eq
                assert(split_by_eq[-2].endswith('&id'))
                taxon_id = split_by_eq[-1]
                assert(str(long(taxon_id)) == str(taxon_id))
                link = PREFIX + link[2:]
                #print 'follow "' + str(link) + '" for "' + str(taxon) + '"'
                contained.append((taxon, taxon_id, link))
            assert(len(contained) > 0)
    children = []
    if contained:
        sys.stderr.write('"%s" is a higher-level taxon\n' % taxonomic_name)
        for sub in contained:
            sub_tax, sub_id, sub_link = sub
            fn = os.path.join(save_dir, sub_id + '.html')
            if os.path.exists(fn):
                sys.stderr.write('"%s" exists. Assuming that it corresponds to "%s"\n' % (fn, sub_tax))
                child_content = open(fn, 'rU').read()
            else:
                sys.stderr.write('Fetching "%s" from "%s" to be saved in "%s"\n' % (sub_tax, sub_link, fn))
                r = requests.get(sub_link)
                child_content = r.content
                fo = open(fn, 'w')
                fo.write(child_content)
                fo.close()
            child = parse_taxon_html(child_content, sub_tax, save_dir, level + 1)
            children.append(child)
    else:
        sys.stderr.write('"%s" is a leaf taxon\n' % taxonomic_name)
    return Node(taxonomic_name, children)
        

if __name__ == '__main__':
    csv_format = ('-t' in sys.argv)
    try:
        initial = sys.argv[1]
        save_dir = sys.argv[2]
    except:
        sys.exit('expecting the path to the starting taxon html file and an output directory')
    html_doc = open(initial, 'rU').read()
    amphib = parse_taxon_html(html_doc, 'Amphibia', save_dir, 0)
    if csv_format:
        amphib.write_csv(sys.stdout, 100000000001)
    else:
        amphib.write_newick(sys.stdout)
        print ';'
        
