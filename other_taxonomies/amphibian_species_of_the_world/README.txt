The classification here was obtained from the following source:
################################################################################
Note: Data presented in Amphibian Species of the World at http://research.amnh.org/vz/herpetology/amphibia/ were updated to be current with ASW's database as of 31 January, 2011.

Trademark Notice: "Amphibian Species of the World: An Online Reference" is a trademark of Darrel R. Frost and the American Museum of Natural History. Any other product or company names mentioned herein are the trademarks of their respective owners.

Copyright Notice: Copyright © 1998-2011 Darrel R. Frost and American Museum of Natural History. All Rights Reserved. Each document delivered from this server or web site may contain other proprietary notices and copyright information relating to that document. The following citation should be used in any published materials which reference the web site.

Citation: Frost, Darrel R. 2011. Amphibian Species of the World: an online reference. Version 5.5 (31 January, 2011). Electronic Database accessible at http://research.amnh.org/vz/herpetology/amphibia/
American Museum of Natural History, New York, USA.
################################################################################
