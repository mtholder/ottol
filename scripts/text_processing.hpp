#if ! defined(TEXT_PROCESSING_HPP)
#define TEXT_PROCESSING_HPP
/**
 * Functions associated with reading the file types and pulling out specific parts of strings
 */

#include <string>
#include <list>

/// Searches s (starting at position pos for the substring delimited by `delim' and ending with ind-th
//		occurrence of delim. Returns false if the search fails to find `ind` copies of delim
//		if it returns true, then output will equal the substring, and pos will be set to
//		the last occurrence of delim;
// This is like 0-based column searching for columns separated by delim
inline bool find_ith(const std::string & s, const char delim, unsigned ind, std::string & output, std::size_t &pos) {
	std::size_t prev_pos = pos;
#	if defined(DEBUGGING_FIND_ITH) && DEBUGGING_FIND_ITH
		std::cerr << " incoming fullstr = \"" << s << "\"\n";
		std::cerr << " delim = \'" << delim << "\'  ind = " << ind<< "  pos = " << pos << "\n";
		std::cerr << " incoming substr = \"" << s.substr(0, pos + 1) << "\"\n";
#	endif
	pos = s.find(delim, pos);
	if (pos == std::string::npos) {
		return false;
	}
	for (unsigned i = 1; i <= ind; ++i) {
		++pos;
		prev_pos = pos;
		pos = s.find(delim, pos);
		if (pos == std::string::npos) {
			return false;
		}
#		if defined(DEBUGGING_FIND_ITH) && DEBUGGING_FIND_ITH
			std::cerr << "substr = \"" << s.substr(0, pos + 1) << "\"\n";
#		endif
	}
	if (pos == prev_pos) {
		output = "";
	}
	else {
		output = s.substr(prev_pos, pos - prev_pos);
	}
#	if defined(DEBUGGING_FIND_ITH) && DEBUGGING_FIND_ITH
		std::cerr << "output = \"" << output << "\"\n";
#	endif
	return true;
}

inline std::size_t skip_ws(const std::string & s, std::size_t start) {
	std::size_t i = start;
	for (; i < s.length(); ++i) {
		if (std::isgraph(s[i])) {
			return i;
		}
	}
	return i;
}

inline std::size_t skip_graph(const std::string & s, std::size_t start) {
	std::size_t i = start;
	for (; i < s.length(); ++i) {
		if (!std::isgraph(s[i])) {
			return i;
		}
	}
	return i;
}

inline void split_str(const std::string &s, std::list<std::string> & r) {
	std::string current;
	for (std::string::const_iterator sIt = s.begin(); sIt != s.end(); ++sIt) {
		const char c = *sIt;
		if (std::isgraph(c))
			current.append(1, c);
		else if (!current.empty()) {
			r.push_back(current);
			current.clear();
		}
	}
	if (!current.empty())
		r.push_back(current);
}


inline void prune_parens_and_abbrev(std::list<std::string> & word_list) {
	std::list<std::string>::const_iterator wIt = word_list.begin();
	std::list<std::string> r;
	bool in_parens = false;
	for (; wIt != word_list.end(); ++wIt) {
		const std::string & word = *wIt;
		if (in_parens) {
			if (word[word.length() - 1] == ')') {
				in_parens = false;
			}
		}
		else if (word[0] == '(') {
			in_parens = true;
		}
		else {
			if ((word != "var.")
				&& (word != "ssp.")
				&& (word != "subsp.")) {
				r.push_back(word);
			}
		}
	}
	word_list = r;
}

inline std::string clean_union4_name(const std::string & name) {
	//std::cerr << "clean_union4_name(" << name << ");\n";

	std::list<std::string> word_list;
	split_str(name, word_list);
	prune_parens_and_abbrev(word_list);
	if (word_list.empty()) {
		return std::string();
	}
	std::list<std::string>::const_iterator wIt = word_list.begin();
	std::string r = *wIt++;
	if (!std::isupper(r[0])) {
		std::cerr << "# Warning starting with parse name lowercase: \"" << r << "\"\n";
	}
	while (wIt != word_list.end() && std::islower((*wIt)[0])) {
		r.append(1, ' ');
		r.append(*wIt);
		++wIt;
	}
	return r;
}




inline bool parse_triple4taxomachine_line(const std::istream& inf, unsigned line_num,
										  const std::string & line,
										  std::string & tax_id,
										  std::string & parent,
										  std::string & name) {
	std::size_t pos = 0;
	if (!find_ith(line, ',', 0, tax_id, pos)) {
		if (inf.good()) {
			std::cerr << "Expecting at least a word before a comma, but did not find them at line " << line_num << '\n';
			exit(1);
		}
		return false; // EOF!
	}
	pos++;
	std::size_t prev_pos = pos;
	if (!find_ith(line, ',', 0, parent, pos)) {
		if (line[prev_pos] != ',') {
			if (inf.good()) {
				std::cerr << "Expecting at least 1 commas, but did not find them at line " << line_num << '\n';
				exit(1);
			}
			return false; // EOF!
		}
		pos = prev_pos;
	}
	pos++;
	name = line.substr(pos, line.length() - pos);
	//std::cerr << "id = " << tax_id << "\n par_id = " << parent << "\n	 name = \"" << name << "\"\n";
	return true;
}


inline bool parse_union4_line(const std::istream& inf, unsigned line_num,
							  const std::string & line,
							  std::string & tax_id,
							  std::string & parent,
							  std::string & name) {
	std::size_t pos = 0;

	if (!find_ith(line, ',', 0, tax_id, pos)) {
		if (inf.good()) {
			std::cerr << "Expecting at least a word before a comma, but did not find them at line " << line_num << '\n';
			exit(1);
		}
		return false; // EOF!
	}
	pos++;
	std::size_t prev_pos = pos;
	if (!find_ith(line, ',', 0, parent, pos)) {
		if (line[prev_pos] != ',') {
			if (inf.good()) {
				std::cerr << "Expecting at least 1 commas, but did not find them at line " << line_num << '\n';
				exit(1);
			}
			return false; // EOF!
		}
		pos = prev_pos;
	}
	pos++;
	prev_pos = pos;
	if (!find_ith(line, ',', 1, name, pos)) {
		if (line[prev_pos] != ',') {
			if (inf.good()) {
				std::cerr << "Expecting at least 3 commas, but did not find them at line " << line_num << '\n';
				exit(1);
			}
			return false; // EOF!
		}
		pos = prev_pos;
	}

	if (name.empty()) {
		if (parent.empty()) {
			name = "life";
		}
	}
	else {
		if (name[0] == '\"') {
			pos = prev_pos + 1;
			if (!find_ith(line, '\"', 1, name, pos)) {
				std::cerr << "Expecting a close quote, but did not find it at line " << line_num << '\n';
				exit(1);
			}
		}
		const std::string in_name = name;
		if (name != "life") {
			name = clean_union4_name(in_name);
		}
	}
	if (name.empty()) {
		std::cerr << "DELETE " << tax_id << "\n";
	}
	//std::cerr << "id = " << tax_id << "\n par_id = " << parent << "\n	 name = \"" << name << "\"\n";
	return true;
}

inline bool parse_ottol_line(const std::istream& inf, unsigned line_num,
							 const std::string & line,
							 std::string & tax_id,
							 std::string & parent,
							 std::string & name) {
	std::size_t pos = 0;
	//std::cerr << "line = \"" << line << "\"\n";
	if (!find_ith(line, '\t', 0, tax_id, pos)) { // get the 1st column (index 0 + skip=0)
		if (inf.good()) {
			std::cerr << "Expecting at least 1 tab, but did not find them at line " << line_num << '\n';
			exit(1);
		}
		return false; // EOF!
	}
	++pos;
	if (!find_ith(line, '\t', 0, parent, pos)) { // get the 2nd column ( pos=1 + skip=0)
		if (inf.good()) {
			std::cerr << "Expecting at least 2 tabs, but did not find them at line " << line_num << '\n';
			exit(1);
		}
		return false; // EOF!
	}
	++pos;
	if (!find_ith(line, '\t', 1, name, pos)) { // get the 4th column (pos=3 + skip = 1)
		std::cerr << "Expecting at least 4 tabs, but did not find them at line " << line_num << '\n';
		exit(1);
	}
	//std::cerr << "id = " << tax_id << "\n par_id = " << parent << "\n	 name = \"" << name << "\"\n";
	return true;
}

#endif
