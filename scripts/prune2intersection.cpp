#include "parse_ottol.hpp"
#include <cassert>
#include <cstring>
#include <fstream>
#include <stack>
#include <algorithm>
#include <vector>

enum NODE_FLAG { OTTOL_INTERNAL  = 0x01,
                 OTTOL_LEAF      = 0x02,
                 OTHER_INTERNAL  = 0x04,
                 OTHER_LEAF      = 0x08,
                 SHARED_LEAF     = 0x0A,
                 ANC_IN_OTTOL    = 0x09,
                 SHARED_INTERNAL = 0x05,
                 ANC_IN_OTHER    = 0x06,
                 IN_OTTOL        = 0x03,
                 IN_OTHER        = 0x0C,
                 };

inline bool in_both(short flag) {
    return ((flag & IN_OTTOL) && (flag & IN_OTHER));
}

// Registers either OTTOL_LEAF or OTTOL_INTERNAL in the Node*
void flag_ottol(Node * nd, std::map<Node *, short> & nd2flag) {
    if (nd == nullptr) {
        return;
    }
    if (nd->children.empty()) {
        nd2flag[nd] = OTTOL_LEAF;
    }
    else {
        nd2flag[nd] = OTTOL_INTERNAL;
        std::list<Node *>::const_iterator childIt = nd->children.begin();
        for (; childIt != nd->children.end(); ++childIt) {
            flag_ottol(*childIt, nd2flag);
        }
    }
}

void map_taxonomy_nodes_by_name(Node * query_root,
                                const std::map<std::string, Node *> & ref_taxonomy,
                                std::map<Node *, Node *> & q2ref_map,
                                std::vector<Node *> & unmatched,
                                std::map<Node *, short> & nd2flag) {
    std::map<std::string, Node *>::const_iterator rtIt = ref_taxonomy.find(query_root->name);
    if (rtIt == ref_taxonomy.end()) {
        unmatched.push_back(query_root);
        if (query_root->children.empty()) {
            nd2flag[query_root] = OTHER_LEAF;
        }
        else {
            nd2flag[query_root] = OTHER_INTERNAL;
        }
    }
    else {
        Node * ottol_node = rtIt->second;
        if (query_root->children.empty()) {
            nd2flag[ottol_node] |= OTHER_LEAF;
        }
        else {
            nd2flag[ottol_node] |= OTHER_INTERNAL;
        }
        q2ref_map[query_root] = ottol_node;
    }
    std::list<Node *>::const_iterator childIt = query_root->children.begin();
    for (; childIt != query_root->children.end(); ++childIt) {
        map_taxonomy_nodes_by_name(*childIt, ref_taxonomy, q2ref_map, unmatched, nd2flag);
    }
}

void define_ottol_clades_with_shared(Node * query_root,
                               const std::map<Node *, short> & nd2flag,
                               std::map<Node *, std::set<Node *> > & clade_to_leaf_set) {
    if (!query_root->children.empty()) {
        std::set<Node *> ls;
        std::list<Node *>::const_iterator childIt = query_root->children.begin();
        for (; childIt != query_root->children.end(); ++childIt) {
            Node * child = *childIt;
            if (child->children.empty()) {
                std::map<Node *, short>::const_iterator n2fIt = nd2flag.find(child);
                assert(n2fIt != nd2flag.end());
                const short f = n2fIt->second;
                if (in_both(f)) {
                    ls.insert(child);
                }
            }
            else {
                define_ottol_clades_with_shared(child, nd2flag, clade_to_leaf_set);
                const std::set<Node *> & cls = clade_to_leaf_set[child];
                ls.insert(cls.begin(), cls.end());
            }
        }
        clade_to_leaf_set[query_root] = ls;
    }
}

void define_other_clades_with_shared(Node * query_root,
                               const std::map<Node *, Node *> & q2ref_map,
                               std::map<Node *, std::set<Node *> > & clade_to_leaf_set) {
    if (!query_root->children.empty()) {
        std::set<Node *> ls;
        std::list<Node *>::const_iterator childIt = query_root->children.begin();
        for (; childIt != query_root->children.end(); ++childIt) {
            Node * child = *childIt;
            if (child->children.empty()) {
                std::map<Node *, Node *>::const_iterator q2rIt = q2ref_map.find(child);
                if (q2rIt != q2ref_map.end()) {
                    ls.insert(q2rIt->second);
                }
            }
            else {
                define_other_clades_with_shared(child, q2ref_map, clade_to_leaf_set);
                const std::set<Node *> & cls = clade_to_leaf_set[*childIt];
                ls.insert(cls.begin(), cls.end());
            }
        }
        clade_to_leaf_set[query_root] = ls;
    }
}

class MissingSizePtr {
    public:
        MissingSizePtr(unsigned missing, unsigned size, Node * p)
            :num_missing(missing),
            tot_size(size),
            nd(p) {
        }
        bool operator<(const MissingSizePtr & other) const {
            if (this->num_missing < other.num_missing) {
                return true;
            }
            if (this->num_missing > other.num_missing) {
                return false;
            }
            if (this->tot_size < other.tot_size) {
                return true;
            }
            if (this->tot_size > other.tot_size) {
                return false;
            }
            return ((long)this->nd) < ((long)other.nd);
        }
        unsigned num_missing;
        unsigned tot_size;
        Node *nd;
};

int main(int argc, char * argv[]) {
    bool quiet = false;
    bool in_triples = true;
    if (argc != 5) {
        std::cerr << "Expecting\n  " << argv[0] << " ottol_triple.txt other_triple.txt taxon outfile\nWhere \"taxon\" is the name of a taxon whose descendants will be retained.\nThe -p flag can be used to handle a file that has already been processed into triples.\n";
        return 1;
    }
    int curr_ind = 1;
    std::string ottol_fn = argv[curr_ind++];
    std::ifstream ottol_infile(ottol_fn);
    if (!ottol_infile.good()) {
        std::cerr << "Could not open\n  " << ottol_fn << "\n";
        return 1;
    }
    std::string other_fn = argv[curr_ind++];
    std::ifstream other_infile(other_fn);
    if (!other_infile.good()) {
        std::cerr << "Could not open\n  " << other_fn << "\n";
        return 1;
    }
    std::string taxon(argv[curr_ind++]);
    std::string outfn = argv[curr_ind++];
    std::ofstream outfile(outfn);
    if (!outfile.good()) {
        std::cerr << "Could not open\n  " << outfn << "\n";
        return 1;
    }
    // Read the first taxonomy (we'll assume that it is OTToL in terms of variable naming...
    std::map<std::string, Node *> ottol_name2node;
    id_to_node ottol_map;
    Node * ottol_root = parse_stream_for_taxon_store_id(ottol_infile, taxon, in_triples, false, quiet, ottol_map, &ottol_name2node);
    if (ottol_root == nullptr) {
        std::cerr << "Taxon \"" << taxon << "\" not found in \"" << ottol_fn << "\" returned. Exiting...\n";
        return 1;
    }

    // Read the other taxonomy...
    id_to_node other_map;
    std::map<std::string, Node *> other_name2node;
    Node * other_root = parse_stream_for_taxon_store_id(other_infile, taxon, in_triples, false, quiet, other_map, &other_name2node);
    if (other_root == nullptr) {
        std::cerr << "Taxon \"" << taxon << "\" not found in \"" << other_fn << "\" returned. Exiting...\n";
        return 1;
    }

    std::map<Node *, Node *> other2ottol;
    std::vector<Node *> unmatched;

    std::set<Node *> ottol_leaf_set;
    fill_leaf_set(ottol_root, ottol_leaf_set);
    std::map<Node *, short> nd2flag;
    flag_ottol(ottol_root, nd2flag);


    std::cerr << ottol_name2node.size() << " names in OTToL\n";
    std::cerr << ottol_leaf_set.size() << " leaves in OTToL\n";

    map_taxonomy_nodes_by_name(other_root,
                               ottol_name2node,
                               other2ottol,
                               unmatched,
                               nd2flag);

    std::cerr << unmatched.size() << " unmatched names in query:\n";
    for (std::vector<Node *>::const_iterator uIt = unmatched.begin(); uIt != unmatched.end(); ++uIt) {
        std::cerr << "  \"" << (*uIt)->name << "\"\n";
    }
    std::cerr << other2ottol.size() << " matched names in query\n";

    std::map<Node *, std::set<Node *> > ottol_clades;
    define_ottol_clades_with_shared(ottol_root, nd2flag, ottol_clades);

    std::map<Node *, std::set<Node *> > other_clades;
    define_other_clades_with_shared(other_root, other2ottol, other_clades);

    std::map<Node *, std::set<Node *> >::const_iterator ncIt = other_clades.begin();
    for (; ncIt != other_clades.end(); ++ncIt) {
        Node * new_internal = ncIt->first;
        const std::set<Node *> & n_shared = ncIt->second;
        const unsigned n_tot_size = n_shared.size();
        std::set<MissingSizePtr> sorted_matches;
        std::map<Node *, std::set<Node *> >::const_iterator ocIt = ottol_clades.begin();

        for (; ocIt != ottol_clades.end(); ++ocIt) {
            Node * ottol_internal = ocIt->first;
            const std::set<Node *> & ottol_shared_members = ocIt->second;
            std::set<Node *> missing_nds;
            std::set_difference(n_shared.begin(), n_shared.end(),
                                ottol_shared_members.begin(), ottol_shared_members.end(),
                                std::inserter(missing_nds, missing_nds.end()));
            unsigned num_missing = missing_nds.size();
            unsigned tot_size = ottol_shared_members.size();
            MissingSizePtr msp(num_missing, tot_size, ottol_internal);
            sorted_matches.insert(msp);
            if (num_missing == 0 && tot_size == n_tot_size) {
                break;
            }
        }
        if (!sorted_matches.empty()) {
            MissingSizePtr best_match = *sorted_matches.begin();
            Node * ottol_node = best_match.nd;
            bool perfect_match = false;
            if (best_match.num_missing == 0) {
                if (best_match.tot_size == n_tot_size) {
                    perfect_match = true;
                    if (new_internal->name == ottol_node->name) {
                        std::cerr << "Clade \"" << ottol_node->name << "\" definition unchanged.\n";
                    }
                    else if (ottol_name2node.find(new_internal->name) == ottol_name2node.end()) {
                        std::cerr << "New clade \"" << new_internal->name << "\" matches \"" << ottol_node->name << "\" definition.\n";
                    }
                    else {
                        std::cerr << "Redefinition for \"" << new_internal->name << "\" matches \"" << ottol_node->name << "\" definition.\n";
                    }
                }
                else {
                    assert (best_match.tot_size > n_tot_size);
                    if (new_internal->name == ottol_node->name) {
                        std::cerr << "New clade definition for \"" << ottol_node->name << "\" is less inclusive than previous definition.\n";
                    }
                    else if (ottol_name2node.find(new_internal->name) == ottol_name2node.end()) {
                        std::cerr << "New clade \"" << new_internal->name << "\" refines \"" << ottol_node->name << "\" definition.\n";
                    }
                    else {
                        std::cerr << "Redefinition of the name \"" << new_internal->name << "\" refines \"" << ottol_node->name << "\" definition.\n";
                    }
                }
            }
            else {
                assert(false);
            }

            if (!perfect_match) {
                const std::set<Node *> & ottol_shared_members = ottol_clades[ottol_node];
                std::set<Node *> new_in_def_shared;
                std::set<Node *> omitted_in_def;
                std::set_difference(n_shared.begin(), n_shared.end(),
                                    ottol_shared_members.begin(), ottol_shared_members.end(),
                                    std::inserter(new_in_def_shared, new_in_def_shared.end()));
                std::set_difference(ottol_shared_members.begin(), ottol_shared_members.end(),
                                    n_shared.begin(), n_shared.end(),
                                    std::inserter(omitted_in_def, omitted_in_def.end()));
                std::cerr << "  New to definition:\n";
                for (auto nIt = new_in_def_shared.begin(); nIt != new_in_def_shared.end(); ++nIt) {
                    std::cerr << "    \"" << (*nIt)->name << "\"\n";
                }
                std::cerr << "  Omitted in new definition:\n";
                for (auto oIt = omitted_in_def.begin(); oIt != omitted_in_def.end(); ++oIt) {
                    std::cerr << "    \"" << (*oIt)->name << "\"\n";
                }
                std::cerr << "\n";
            }
        }
    }
    return 0;
}

