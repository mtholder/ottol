#include "parse_ottol.hpp"
#include <cstring>
#include <fstream>
#include <stack>



void write_childless_uninomials(const BoolNode * nd, std::set<std::string> & out) {
    if (nd == nullptr) {
        return;
    }
    if (nd->children.empty()) {
        if (nd->name.find(' ') == std::string::npos) {
            out.insert(nd->name);
        }
    }
    else {
        std::list<BoolNode *>::const_iterator childIt = nd->children.begin();
        for (; childIt != nd->children.end(); ++childIt) {
            write_childless_uninomials(*childIt, out);
        }
    }
}

int main(int argc, char * argv[]) {
    bool quiet = false;
    bool in_triples = true;
    if (argc < 2 ||  argc > 3) {
        std::cerr << "Expecting\n  " << argv[0] << " taxa.txt [taxon]\nwhere \"taxa.txt\" is an triple taxonomy file and \"taxa\" is an optional taxon name.\n";
        return 1;
    }
    std::ifstream infile(argv[1]);
    if (!infile.good()) {
        std::cerr << "Could not open\n  " << argv[1] << "\n";
        return 1;
    }
    std::string taxon("life");
    if (argc > 2) {
        taxon = argv[2];
    }

    BoolNode * root = BoolNode::parse_stream_for_taxon(infile, taxon, in_triples, false, quiet);
    if (root == nullptr) {
        return 1;
    }
    std::set<std::string> cu;
    write_childless_uninomials(root, cu);
    for (std::set<std::string>::const_iterator cIt = cu.begin(); cIt != cu.end(); ++cIt) {
        std::cout << *cIt << '\n';
    }
    return 0;
}

