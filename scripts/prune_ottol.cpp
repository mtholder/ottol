#include "parse_ottol.hpp"
#include <cstring>
#include <fstream>
#include <stack>
#include <sstream>

int main(int argc, char * argv[]) {
    int flag_index = -1;
    bool quiet = false;
    bool in_triples = false;
    bool union4format = false;
    for (int i = 1; i < argc; ++i) {
        if (std::strcmp(argv[i], "-p") == 0) {
            flag_index = i;
            in_triples = true;
        }
        else if (std::strcmp(argv[i], "-u") == 0) {
            flag_index = i;
            union4format = true;
        }
    }
    if (in_triples && union4format) {
        std::cerr << "The -u and -p flags should not both be used in the same invocation\n";
        return 1;
    }
    if (((flag_index >= 0) && argc != 5) || ((flag_index < 0) && argc !=4)) {
        std::cerr << "Expecting\n  " << argv[0] << " taxa.txt taxon outfile\nWhere \"taxon\" is the name of a taxon whose descendants will be retained.\nThe -p flag can be used to handle a file that has already been processed into triples. Use -u for the union4 core.csv format.\n";
        return 1;
    }
    int curr_ind = 1;
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::ifstream infile(argv[curr_ind++]);
    if (!infile.good()) {
        std::cerr << "Could not open\n  " << argv[1] << "\n";
        return 1;
    }
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::string taxon(argv[curr_ind++]);
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::ofstream outfile(argv[curr_ind++]);
    if (!outfile.good()) {
        std::cerr << "Could not open\n  " << argv[2] << "\n";
        return 1;
    }

    BoolNode * root = BoolNode::parse_stream_for_taxon(infile, taxon, in_triples, union4format, quiet);
    if (root == nullptr) {
        std::cerr << "Taxon \"" << taxon << "\" not found!\n";
        return 2;
    }

    std::stack<BoolNode *> nd_stack;
    outfile << "1,0,root\n";
    std::set<std::string> lines;
    std::stringstream strout;

    strout << root->taxon_index << "," << 1 << "," << root->name << "\n"; // zero out the ancestor for our pseudo-root node
    lines.insert(strout.str());
    std::list<BoolNode *>::reverse_iterator rcIt = root->children.rbegin();
    for (; rcIt != root->children.rend(); ++rcIt) {
        nd_stack.push(*rcIt);
    }
    BoolNode * curr_nd = nd_stack.top();
    nd_stack.pop();

    unsigned num_written=0;
    for (;;) {
        ++num_written;
        std::stringstream strout2;
        strout2 << curr_nd->taxon_index << "," << curr_nd->par_index << "," << curr_nd->name << "\n";
        lines.insert(strout2.str());

        if (curr_nd->children.empty()) {
            if (nd_stack.empty()) {
                break;
            }
        }
        else {
            std::list<BoolNode *>::reverse_iterator cIt = curr_nd->children.rbegin();
            for (; cIt != curr_nd->children.rend(); ++cIt) {
                nd_stack.push(*cIt);
            }
        }
        curr_nd = nd_stack.top();
        nd_stack.pop();
    }
    for (std::set<std::string>::const_iterator sIt = lines.begin(); sIt != lines.end(); ++sIt) {
        outfile << *sIt;
    }
    std::cerr << "# " <<  num_written << " taxonomic names written.\n";
    return 0;
}

