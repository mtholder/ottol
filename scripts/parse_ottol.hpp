#if !defined(PARSE_OTTOL_HPP)
#define PARSE_OTTOL_HPP

#include <list>
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

typedef std::unordered_set<long> indices_to_print;
typedef std::unordered_set<std::string> str_collection;


///
// A simple, generic node class. You can specify the datastructure (of type T)
//	that will be present in each node as `blob`
// T must be default constructable.
template <typename T>
class Node {
	public:
		typedef std::unordered_map<long, Node<T> *> id_to_node_t;

		void write_newick(std::ostream & outfile) const;

        static Node<T> * create() {
            return new Node<T>();
        }
		static Node<T> * parse_stream_for_taxon(std::istream & infile,
								const std::string & taxon,
								bool in_triples,
								bool union4format,
								bool quiet,
								std::map<std::string, Node<T> *> * name_to_node_map_ptr=0L);

		static Node<T> * parse_stream_for_taxon_store_id(std::istream & infile,
								const std::string & taxon,
								bool in_triples,
								bool union4format,
								bool quiet,
								id_to_node_t & id2node,
								std::map<std::string, Node *> * name_to_node_map_ptr=0L);

		static Node<T> * read_names(std::istream & inf,
						  id_to_node_t & id2node,
						  str_collection &all_parents,
						  str_collection &dup_names,
						  const std::string & taxon_to_find,
						  bool in_triples,
						  bool union4format,
						  std::map<std::string, Node<T> *> * name_to_node_map_ptr);

		void fill_leaf_set(std::set<Node<T> *> & leaf_set);


		long taxon_index;
		long par_index;
		std::string name;
		std::list<Node<T> *> children;
		T blob;
};
typedef Node<bool> BoolNode; // minimal memory if you don't need any struct

const unsigned long g_num_hash_buckets = 5500000;
/// Impementation (in header because it is inlined/templated).

#include "text_processing.hpp"

//@TODO: massive memory leak caused by not deleting any nodes
template <typename T>
Node<T> * Node<T>::read_names(std::istream & inf,
				  std::unordered_map<long, Node<T> *> & id2node,
				  str_collection &all_parents,
				  str_collection &dup_names,
				  const std::string & taxon_to_find,
				  bool in_triples,
				  bool union4format,
				  std::map<std::string, Node<T> *> * name_to_node_map_ptr) {
	str_collection all_names(g_num_hash_buckets);
	std::string line;
	unsigned line_num = 1;
	if (union4format) {
		std::getline(inf, line);
		++line_num;
	}
	Node<T> * clade_root = nullptr;
	for(;;) {
		++line_num;
		line.clear();
		std::getline(inf, line);
		std::string tax_id;
		std::string parent;
		std::string name;
		bool parsed_line = true;
		if (in_triples) {
			parsed_line = parse_triple4taxomachine_line(inf, line_num, line, tax_id, parent, name);
		}
		else if (union4format) {
			parsed_line = parse_union4_line(inf, line_num,  line, tax_id, parent, name);
		}
		else {
			parsed_line = parse_ottol_line(inf, line_num,  line, tax_id, parent, name);
		}
		if (!parsed_line) {
			return clade_root; // EOF!
		}
		//std::cerr << "Line " << line_num << ":  parent=\"" << parent << "\" name=\"" << name << "\"\n";
		if (line_num % 100000 == 0) {
			std::cerr << "# " << line_num << '\n';
		}
		Node<T> * nd = Node<T>::create();
		nd->name = name;
		long nd_num = std::atol(tax_id.c_str());
		nd->taxon_index = nd_num;
		if (parent.empty()) {
			nd->par_index = -1;
			if (taxon_to_find.empty()) {
				clade_root = nd;
			}
		}
		else {
			nd->par_index = std::atol(parent.c_str());
		}
		id2node[nd_num] = nd;

		if (all_names.find(name) != all_names.end() ) {
			dup_names.insert(name);
		}
		if (!taxon_to_find.empty()) {
			if (name == taxon_to_find) {
				clade_root = nd;
			}
		}
		all_names.insert(name);
		all_parents.insert(parent);
		if (name_to_node_map_ptr) {
			(*name_to_node_map_ptr)[name] = nd;
		}
	}
	return clade_root;
}

template <typename T>
Node<T> * Node<T>::parse_stream_for_taxon(std::istream & infile,
							  const std::string & taxon_to_find,
							  bool in_triples,
							  bool union4format,
							  bool quiet,
							  std::map<std::string, Node<T> *> * name_to_node_map_ptr) {
	id_to_node_t id2node;
	return parse_stream_for_taxon_store_id(infile,
										   taxon_to_find,
										   in_triples,
										   union4format,
										   quiet,
										   id2node,
										   name_to_node_map_ptr);
}

template <typename T>
Node<T> * Node<T>::parse_stream_for_taxon_store_id(std::istream & infile,
									   const std::string & taxon_to_find,
									   bool in_triples,
									   bool union4format,
									   bool quiet,
									   std::unordered_map<long, Node<T> *> & id2node,
									   std::map<std::string, Node<T> *> * name_to_node_map_ptr) {
	str_collection all_parents(g_num_hash_buckets);
	str_collection dup_names(300000);
	Node<T> * root = read_names(infile, id2node, all_parents, dup_names, taxon_to_find, in_triples, union4format, name_to_node_map_ptr);
	if (root == nullptr) {
		if (!quiet) {
			std::cerr << "Taxon \"" << taxon_to_find << "\" not found!\n";
		}
		return nullptr;
	}

	for (auto mIt = id2node.begin(); mIt != id2node.end(); ++mIt) {
		Node<T> * nd = mIt->second;
		if (id2node.find(nd->par_index) == id2node.end()) {
			if (nd->par_index > 0 && !quiet) {
				std::cerr << "# Parent " << nd->par_index << " not registered in map\n";
				std::cerr << "DELETE " << nd->taxon_index << "\n";
			}
		}
		else {
			Node<T> * par = id2node[nd->par_index];
			par->children.push_back(nd);
		}
	}
	if (root->children.empty() && !quiet) {
		std::cerr << "Taxon \"" << taxon_to_find << "\" has no children. That is not much of a tree!\n";
	}
	if (!quiet) {
		std::cerr << "# back from read_names. " << dup_names.size() << " non unique names\n";
	}
	return root;
}

template <typename T>
void Node<T>::write_newick(std::ostream & out) const {
	if (!this->children.empty()){
		out << '(';
		auto cIt = this->children.begin();
		const Node<T> * nd = *cIt;
		nd->write_newick(out);
		for (++cIt; cIt != this->children.end(); ++cIt) {
			nd = *cIt;
			out << ',';
			nd->write_newick(out);
		}
		out << ')';
	}
	out << '\'' << this->name << '\'';
}


template <typename T>
void Node<T>::fill_leaf_set(std::set<Node *> & leaf_set) {
	if (this->children.empty()) {
		leaf_set.insert(this);
	}
	else {
		auto childIt = this->children.begin();
		for (; childIt != this->children.end(); ++childIt) {
			(*childIt)->fill_leaf_set(leaf_set);
		}
	}
}



#endif // PARSE_OTTOL_HPP
