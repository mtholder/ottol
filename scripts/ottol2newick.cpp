#include "parse_ottol.hpp"
#include <cstring>
#include <fstream>
#include <stack>

int main(int argc, char * argv[]) {
    int flag_index = -1;
    bool quiet = false;
    bool in_triples = false;
    for (int i = 1; i < argc; ++i) {
        if (std::strcmp(argv[i], "-p") == 0) {
            flag_index = i;
            in_triples = true;
        }
    }
    if ((in_triples && argc != 5) || ((!in_triples) && argc !=4)) {
        std::cerr << "Expecting\n  " << argv[0] << " taxa.txt taxon outfile\nWhere \"taxon\" is the name of a taxon whose descendants will be retained.\nThe -p flag can be used to handle a file that has already been processed into triples.\n";
        return 1;
    }
    int curr_ind = 1;
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::ifstream infile(argv[curr_ind++]);
    if (!infile.good()) {
        std::cerr << "Could not open\n  " << argv[1] << "\n";
        return 1;
    }
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::string taxon(argv[curr_ind++]);
    if (curr_ind == flag_index) {
        ++curr_ind;
    }
    std::ofstream outfile(argv[curr_ind++]);
    if (!outfile.good()) {
        std::cerr << "Could not open\n  " << argv[2] << "\n";
        return 1;
    }

    BoolNode * root = BoolNode::parse_stream_for_taxon(infile, taxon, in_triples, false, quiet);
    if (root == nullptr) {
        std::cerr << "Taxon \"" << taxon << "\" not returned. Exiting...\n";
        return 1;
    }
    root->write_newick(outfile);
    return 0;
}

