OTToL Issues
============
This file documents issues with OTToL that need to be cleared up

Values for "rank" column are idiosyncratic
==========================================
Current values for the "rank" field include:

 *  Buprestioid lineage
 *  above family
 *  class
 *  family
 *  genus
 *  order
 *  phylum
 *  regn.
 *  species
 *  subfamily
 *  subgenus
 *  suborder
 *  subspecies
 *  subsubfamily
 *  subtribe
 *  superclass
 *  superfamily
 *  supertribe
 *  tribe
 *  unranked

Uninomials without children
===========================
The childless_uninomials executable looks leaves of the tree that do not have 
a space in their name. These are a bit suspicious, because we expect only 
binomens or trinomens for most taxa (viruses would be one exception). The
"make check" target at the highest level checks the output of 
childless_uninomials against expected_childless_uninomials.txt to identify
cases that are unexpected. The childless_uninomials_notes.txt file has some
the results of googling around for a few of the cases to get a sense of where
these are coming from (seems to be mixture of invalid names and cases of valid/
correct genus names in which the species simply have not been added to OTToL).
