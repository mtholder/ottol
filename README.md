OTToL goals
===========
This repository contains a tab-delimited text file of taxonomic names. The goal
is to place an exhaustive list of currently recognized species in a taxonomic
hiearchy. The goal is to include appropriate name for each species, not list
all synonyms. 


OTToL caveats
=============
This repo is being used by members of the [Open Tree of Life project](http://opentreeoflife.org/)
The use of git as the datastore is a temporary measure to keep project 
participants in sync. We certainly welcome comments about the taxonomy, but be
aware that none of the names or id's in this taxonomy are considered final (e.g.
we may use the taxon id numbers in the file as identifiers the short term, but
those are not intended to be the final ID for a taxon in the OpenTree software
system).


OTToL usage
===========
The [process_ottol_taxonomy.py](https://github.com/OpenTreeOfLife/opentree-treemachine/blob/master/data/process_ottol_taxonomy.py)
script condense the full file into a comma-separated file that can be input
as a taxonmy in the opentree-treemachine system (see the [taxonomy and the graphdb](https://docs.google.com/document/d/1J82ZvgqMwv9Y43SqSGcw1ZjqWEPHaFQww5deuFFV7Js/edit) doc).

    python process_ottol_taxonomy.py OTToL.txt OTToL_processed.txt

will create the processed form as OTToL_precessed.txt

File structure
==============
The OTToL is tab-delimited.  The columns are:

 1.  a unique # for the taxonomic name. The longer names that start with (6000,
     7000, or 9000) have no relationship to an identifier used in another database. 
     Some of the other IDs match an id from another database. But you should just 
     use the identifier as an opaque handle for the taxon.
 2.  The parent's ID
 3.  An underscore-separated code for the source of the name: first there will
     be a code identifying the source of the name (IRMNG, U4, ncbi, or CoL), 
     followed by a tag that is a data-of-access identifier. A suffix of "_PDB"
     indicates that taxon appears in the paleoDB. A suffix of "_hom" indicates
     that the name is known to be a homonym of another (valid or correct) name.
 4.  The name
 5.  The name with authority info (if available, this column is often empty).
 6.  Rank
 7.  Date of access



Original commit
===============
OTToL.txt is a tab-delimited file identical to the OTToL090512 version of the 
OTToL taxonomy produced by Dr. Laura Katz and Dr. Jessica Grant 

Sources of names:

  *   [IRMNG](http://www.obis.org.au/irmng/)
  *   [Union4](http://gnaclr.globalnames.org/)
  *   [ncbi](http://www.ncbi.nlm.nih.gov/taxonomy)
  *   [CoL](http://www.catalogueoflife.org/)
  *   [PaleoDB](http://paleodb.org/cgi-bin/bridge.pl)


Issues/Notes
============
See the "issues" subdirectory

