SUBDIRS = scripts

PRODUCTS = OTToL4taxomachine.txt

all: $(PRODUCTS)
	for dir in $(SUBDIRS); do $(MAKE) -C $$dir; done

.PHONY: all

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

scripts/% :
	$(MAKE) -C scripts
    
OTToL4taxomachine.txt : OTToL.txt scripts/ottol2taxomachine
	echo "Building OTToL4taxomachine.txt from OTToL.txt using ottol2taxomachine..."
	scripts/ottol2taxomachine OTToL.txt life OTToL4taxomachine.txt


clean:
	for dir in $(SUBDIRS); do $(MAKE) -C $$dir clean; done
	rm -f $(PRODUCTS)

childless_uninomials.txt : OTToL4taxomachine.txt scripts/childless_uninomials
	echo "Checking for leaf nodes that are uninomials..."
	scripts/childless_uninomials OTToL4taxomachine.txt life > childless_uninomials.txt

check : childless_uninomials.txt
	if ! diff issues/expected_childless_uninomials.txt childless_uninomials.txt > issues/unexpected_childless_uninomials.txt ; \
	then \
		echo ; echo "Unexpected uninomial taxonomic names found as leaf nodes in the taxonomy"; \
		echo "See issues/unexpected_childless_uninomials.txt for the diff output which compares issues/expected_childless_uninomials.txt to childless_uninomials.txt" ; \
		false ; \
	fi
